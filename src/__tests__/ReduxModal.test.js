import React from 'react';
import { mount } from 'enzyme';
import ReduxModal from '../components/ReduxModal/ReduxModal';

test('Test ReduxModal Component', () => {
    const wrapper = mount(
        <ReduxModal />
      );
      const p = wrapper.find('#ReduxModal');
      expect(p.find('input').exists()).toBe(true);
});