import Landing from '../sections/Landing/Landing';
import React from 'react';
import { mount } from 'enzyme';

test('Test Landing Section', () => {
    const wrapper = mount(
        <Landing />
      );
      const p = wrapper.find('#Landing');
      expect(p.find('input').exists()).toBe(true);
});