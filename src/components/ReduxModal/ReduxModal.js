import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';
import store from '../../store';



export default class ReduxModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  componentDidMount() {

  }

  componentWillUnmount() {
    
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ text: nextProps.response.response.text });
  }

  render() {
    const props = this.props;

    return (
      <div id="ReduxModal">
        <Alert bsStyle="warning">
          Server Response: {this.state.text}
        </Alert>
      </div>
    );
  }
}

ReduxModal.propTypes = {
  portrait: PropTypes.bool
};

ReduxModal.defaultProps = {
  portrait: true
};
