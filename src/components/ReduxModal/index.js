import { connect } from 'react-redux';
import connectTransitionWrapper from '../../decorators/connectTransitionWrapper';
import ReduxModal from './ReduxModal';
import { setResponse } from '../../store/actions/landing';

const mapStateToProps = (state, ownProps) => {
  return {
    response: state.response
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

@connectTransitionWrapper()
@connect(
  mapStateToProps,
  mapDispatchToProps,
  undefined,
  {withRef: true}
)

export default class ReduxModalWrapper extends ReduxModal {
  constructor(props) {
    super(props);
  }
}
