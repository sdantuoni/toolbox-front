import Landing from '../sections/Landing/';

export default [
  {
    key: 'landing',
    Component: Landing,
    path: {
      path: '/',
      exact: true
    }
  }
];
