import React from 'react';
import animate from 'gsap';
import PropTypes from 'prop-types';
import { Alert, Glyphicon, FormGroup, FormControl, ControlLabel, HelpBlock, Button } from 'react-bootstrap';
import ReduxModal from '../../components/ReduxModal';

class Landing extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      apiErr: false,
      value: '',
      apiState: 'ready',
      count: 0,
      url: '', //without protocol
      port: ''
    };
  }

  componentDidMount() {
    animate.set(this.container, {autoAlpha: 0});
    this.getApiState();
    this.interval = setInterval(() => {
      this.getApiState();
    }, 30000);
   
  }

  componentWillAppear(done) {
    this.animateIn(done);
  }

  componentWillEnter(done) {
    this.animateIn(done);
  }

  componentWillLeave(done) {
    this.animateOut(done);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  animateIn = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 1, onComplete});
  };

  animateOut = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 0, onComplete});
  };

  getApiState() {
    const _this = this;
    fetch(`http://localhost:3005/`).then(function(res){
      if(res.status === 200) {
        _this.setState({ 
          apiState: 'ready',
          apiErr: false
        });
      }
    }).catch(function(err){
      _this.setState({ apiState: 'failure' });
    });
  }

  
  sendData() {
    const _this = this;
    fetch(`//${this.state.url}:${this.state.port}/toolbox/${this.state.value}`).then(function(res){
      return res.json();
    })
    .then(function(res){
      if(res.success){
        _this.setState({ 
          count: _this.state.count + 1
         });
        _this.props.setResponse(res);
      }
    })
    .catch(function(err){
      _this.setState({ 
        apiState: 'failure',
        apiErr: true
       });
       setTimeout(function(){
        _this.getApiState();
       }, 5000)
    });
  }

  getValidationState() {
    const length = this.state.value.length;
    if (length >= 3) return 'success';
    else if (length > 0) return 'error';
    return null;
  }

  handleInputChange(event, name) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState({
      [name]: value
    });
  }


  render() {
    const props = this.props;
    const style = Object.assign({}, props.style);

    return (
      <main
        id="Landing"
        style={style}
        ref={r => this.container = r}
      >
        {this.state.apiErr &&   
          <Alert bsStyle="danger">
            <strong>Sorry, could not connect with the server.</strong> Reconnecting ...
          </Alert>
        }

        {this.state.count > 0 && <ReduxModal />}

        <h2 className="title">Please, insert a text to test the <span className={this.state.apiState}>API <Glyphicon className={this.state.apiState} glyph="tasks" /></span></h2>

        <FormGroup
          controlId="formBasicText"
          className="form"
          validationState={this.getValidationState()}
        >
          <ControlLabel>{this.state.apiState !== 'ready' ? 'The Server Didn´t Respond' : 'The Server Is Ready'}</ControlLabel>
          <FormControl
            type="text"
            value={this.state.value}
            placeholder="Enter text"
            onChange={(e) => this.handleInputChange(e, 'value')}
          />
          <FormControl.Feedback />
          <HelpBlock>Validation is based on string length.</HelpBlock>
        </FormGroup>

        <Button className="submit" disabled={this.state.apiState !== 'ready' ? true : false} bsStyle="primary" onClick={() => this.sendData()}>Enviar</Button>

      </main>
    );
  }
}

Landing.propTypes = {
  style: PropTypes.object,
  windowWidth: PropTypes.number,
  windowHeight: PropTypes.number,
  setResponse: PropTypes.func.isRequired
};

Landing.defaultProps = {
  style: {},
};

export default Landing;
