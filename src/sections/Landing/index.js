import { connect } from 'react-redux';
import connectTransitionWrapper from '../../decorators/connectTransitionWrapper';
import Landing from './Landing';
import { setResponse } from '../../store/actions/landing';

const mapStateToProps = (state, ownProps) => {
  return {
    response: state.response
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setResponse: val => dispatch(setResponse(val)),
  };
};

@connectTransitionWrapper()
@connect(
  mapStateToProps,
  mapDispatchToProps,
  undefined,
  {withRef: true}
)

export default class LandingWrapper extends Landing {
  constructor(props) {
    super(props);
  }
}
