import keys from '../keys';

export const setResponse = function(response) {
  return {
    type: keys.API_RESPONSE,
    response,
  };
};