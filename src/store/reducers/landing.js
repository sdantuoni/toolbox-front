import keys from '../keys';

export const response = function(state = false, action) {
  switch (action.type) {
    case keys.API_RESPONSE:
      return action;
    default:
      return state;
  }
};